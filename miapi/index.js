const express = require("express"); // es el nostre servidor web
const cors = require("cors"); // ens habilita el cors recordes el bicing???
const bodyParser = require("body-parser"); // per a poder rebre jsons en el body de la resposta
const app = express();
const multer = require("multer");
const storage = multer.diskStorage({
  destination: "../upload",
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + file.originalname);
  },
});
const upload = multer({ storage: storage });
const baseUrl = "/miapi";
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd
ddbbConfig = {
  user: "sergi-garriga-7e3",
  host: "postgresql-sergi-garriga-7e3.alwaysdata.net",
  database: "sergi-garriga-7e3_bbdd",
  password: "ITBsergiGM",
  port: 5432,
};
//El pool es un congunt de conexions
const Pool = require("pg").Pool;
const pool = new Pool(ddbbConfig);

//veure motos
const getMotos = (request, response) => {
  var consulta = "SELECT * FROM  motos";
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
    console.log(results.rows);
  });
};
app.get(baseUrl + "/getmotos", getMotos);

//-------------------

//Filtre
const capitalize = (s) => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};
const getMotosF = (request, response) => {
  const marca = capitalize(request.query.marca);

  var consulta = `SELECT * FROM  motos WHERE marca = '${marca}'`;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.get(baseUrl + "/getmotosf", getMotosF);

//-------------------

//add Moto
const addMoto = (request, response) => {
  console.log(request.body);
  const { marca, modelo, year, foto, precio } = request.body;
  console.log(marca, modelo, year, foto, precio);
  //response.send("Hola aixo es el que m'arriva" + JSON.stringify(request.body));
  var consulta = `insert into motos (marca,modelo,year,precio,foto) values ('${marca}','${modelo}','${year}','${precio}','${foto}') `;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.post(baseUrl + "/moto", addMoto);

//add Moto Foto
const addMotoF = (request, response) => {
  console.log(request.body);
  console.log(request.files);
  const { marca, modelo, year, precio } = request.body;
  const { foto } = request.files[0].path;
  console.log(marca, modelo, request.files[0].path, year, precio);
  //response.send("Hola aixo es el que m'arriva " + JSON.stringify(request.body));
  //console.log(foto);

  var consulta = `insert into motos (marca,modelo,year,precio,foto) values ('${marca}','${modelo}','${year}','${precio}€','http://sergi-garriga-7e3.alwaysdata.net/upload/${request.files[0].filename}') `;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

app.post(baseUrl + "/motoF", upload.any(), addMotoF);

//delete motos
const deleteMoto = (request, response) => {
  console.log(request.query.id);

  var consulta = `delete FROM motos where id ='${request.query.id}'`;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.delete(baseUrl + "/delete", deleteMoto);

//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
  console.log("El servidor está inicialitzat en el puerto " + PORT);
});
