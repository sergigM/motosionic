import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-moto',
  templateUrl: './moto.page.html',
  styleUrls: ['./moto.page.scss'],
})
export class MotoPage implements OnInit {
  data: any = [];
  motosArray: any = [];
  respuesta: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public alertController: AlertController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        console.log();
        this.data = this.router.getCurrentNavigation().extras.state.parametro;
        console.log(this.data);
      }
    });
  }
  async deleteMoto(id: number) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar Moto',
      message: 'Quieres eliminar la foto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Borrar',
          handler: async () => {
            await fetch(
              'http://sergi-garriga-7e3.alwaysdata.net/miapi/delete?id=' + id,
              {
                method: 'DELETE',
              }
            ).then((response) => this.router.navigate(['home']));
            console.log('Confirm Okay');
          },
        },
      ],
    });

    await alert.present();
  }

  ngOnInit() {}
}
