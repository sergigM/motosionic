import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  motosArray: any = [];
  respuesta: any;

  constructor(private router: Router, private menu: MenuController) {
    this.getJson();
  }

  async getJson() {
    this.respuesta = await fetch(
      'http://sergi-garriga-7e3.alwaysdata.net/miapi/getmotos'
    );
    this.motosArray = await this.respuesta.json();
    console.log(this.motosArray);
  }

  a(moto) {
    let navigationExtras: NavigationExtras = {
      state: {
        parametro: moto,
      },
    };
    this.router.navigate(['moto'], navigationExtras);
  }
  newMoto() {
    this.router.navigate(['altamotofoto']);
  }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  ionViewWillEnter() {
    this.getJson();
  }
  async getJsonF(marca) {
    this.respuesta = await fetch(
      `http://sergi-garriga-7e3.alwaysdata.net/miapi/getmotosf?marca=${marca}`
    );
    this.motosArray = await this.respuesta.json();
    console.log(this.motosArray);
  }
}
