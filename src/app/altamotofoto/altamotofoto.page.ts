import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-altamotofoto',
  templateUrl: './altamotofoto.page.html',
  styleUrls: ['./altamotofoto.page.scss'],
})
export class AltamotofotoPage implements OnInit {
  image: any;
  moto: any = [];

  respuesta: any;
  constructor(private router: Router) {
    //this.form.append(this.a);
  }

  algons() {
    const form = new FormData();
    this.image = (<HTMLInputElement>(
      document.getElementsByName('foto')[0]
    )).files[0];
    form.append('foto', this.image);
    form.append('marca', this.moto.marca);
    form.append('modelo', this.moto.model);
    form.append('year', this.moto.year);
    form.append('precio', this.moto.precio);
    console.log(this.image);
    fetch('http://sergi-garriga-7e3.alwaysdata.net/miapi/motoF', {
      method: 'post',
      body: form,
    }).then((response) => {
      console.log(response);
      this.router.navigate(['home']);
    });
  }

  ngOnInit() {}
}
